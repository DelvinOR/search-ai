import random
import numpy as np
import math
from copy import copy, deepcopy
import time

dim = 5
prob = .25

def renderMap (dim, prob):
    
    # Create dim x dim grid
    grid = []

    # Create dim rows
    for row in range(dim):
       # Create dim cols (2D Array)
        grid.append([])
        for col in range(dim):
            # Place values 
            if (prob < random.random()):
                grid[row].append('-')
            else:
                grid[row].append('X')
            
    grid[0][0] = 'S'
    grid[dim-1][dim-1] = 'G'
    
    return grid

map = renderMap(dim, prob)
maporig = deepcopy(map)

def euclidDist(fst, snd):
    dist = math.sqrt((fst[0]-snd[0])**2 + (fst[1]-snd[1])**2)
    return dist
    
def manhatDist(fst, snd):
    dist = float(abs(fst[0]-snd[0]) + abs(fst[1]-snd[1]))
    return dist
    
# get neighbors given current location and dim in order down, right, up,
# right visually from 0,0 top left to dim-1,dim-1 bottom right
def getNeibs(row, col, dim):
    # coordinates cant be less than 0 or greater than dim - 1 return none if so
    if (row < 0 or row > dim - 1 or col < 0 or col > dim -1):
        return
    # anywhere inside of the outer 'ring'
    if ((0 < row < dim - 1) and (0 < col < dim - 1)):
        neibs = [[row+1,col],[row,col+1],[row-1,col],[row,col-1]]
        return neibs
    # top row
    elif (row == 0):
        # top left corner
        if (col == 0):
            neibs = [[row+1,col],[row,col+1]]
            return neibs
        # top right corner
        elif (col == dim - 1):
            neibs = [[row+1,col],[row,col-1]]
            return neibs
        # rest of top row
        else:
            neibs = [[row+1,col],[row,col+1],[row,col-1]]
            return neibs
    # bottom row
    elif (row == dim - 1):
        # bottom left
        if (col == 0):
            neibs = [[row,col+1],[row-1,col]]
            return neibs
        # bottom right - commonly GOAL
        elif (col == dim-1):
            neibs = [[row-1,col],[row,col-1]]
            return neibs
        # rest of bottom row
        else:
            neibs = [[row,col+1],[row-1,col],[row,col-1]]
            return neibs
    # left column
    elif (col == 0):
        # top and bottom square already covered above
        neibs = [[row+1,col],[row,col+1],[row-1,col]]
        return neibs
    # last column
    else:
        #top and bottom square already covered above
        neibs = [[row+1,col],[row-1,col],[row,col-1]]
        return neibs

# end getNeibs

print("------ORIGINAL MAP------")
print(np.matrix(map))

# creates visited tracking array
def mkVisAry(dim):
    visited = []
    # Create dim rows
    for row in range(dim):
       # Create dim cols (2D Array)
        visited.append([])
        for col in range(dim):
            # Place values 
            visited[row].append(False)
    visited[0][0] = True
    return visited

# Part 3

# Defining a fitness function as the manhattan distance from curr coordinate to goal cell
# The lesser the result, the better curr is
def fit(curr):
    return manhatDist(curr, [dim-1,dim-1])

def localSearchOne(maze):
    visited = mkVisAry(dim)
    visited[0][0] = False
    start = [0,0]
    if start == [dim-1,dim-1]:
        return 0

    currState = start
    pathCount = 1
    while maze[currState[0]][currState[1]] != 'G':
        # If the new currState is visited already, that means we are visiting a node that has already been explored
        if visited[currState[0]][currState[1]]:
            break
        else:
            visited[currState[0]][currState[1]] = True
        neibs = getNeibs(currState[0], currState[1], len(maze))
        for n in neibs:
            # if it is not visited and cell is not 'X', we can check fitness
            if visited[n[0]][n[1]] == False and maze[n[0]][n[1]] != 'X':
                if fit(n) < fit(currState):
                    currState = n.copy()
                    pathCount+=1
                    if maze[currState[0]][currState[1]] == 'G':
                        return pathCount
                break
    return -1

def dfsMaxFringe(maze):
    # array for tracking visited cells
    visited = mkVisAry(dim)
    
    # fringe of tuples containing coordinates and path to coordinate
    fringeLimit = 5
    fringe = [([0,0], [])] 
    
    # loop while the fringe isn't empty
    while fringe:
        cell, path = fringe.pop()
        path.append(cell)
        visited[cell[0]][cell[1]] = True

        if maporig[cell[0]][cell[1]] == 'G':
            #for i in range(dim):
                #for j in range(dim):
                    #if (visited[i][j] == True):
                        #visit[i][j] = 'T'
                    #else:
                        #visit[i][j] = 'F'
            #print(np.matrix(visit))
            return len(path)

        neibs = getNeibs(cell[0],cell[1],dim)
        neibs.reverse()
        for neib in neibs:
            if visited[neib[0]][neib[1]] == False and map[neib[0]][neib[1]] != 'X':
                if len(fringe) <= fringeLimit:
                    fringe.append((neib, path[:]))
    # no path found            
    return -1  

# fringe sort based on F function and heuristic tie breaker
def fringeSort(fringe):
    # sort fringe based on F function = sum of path length to cell and heuristic
    fringe.sort(key = lambda x: len(x[1]) + x[2])
    # sort ties using heuristic as tie breaker
    for i in range(0,len(fringe)-1):
        for j in range(i+1,len(fringe)):
            # F functions are the same
            if ((len(fringe[i][1]) + fringe[i][2]) == (len(fringe[j][1]) + fringe[j][2])):
                # order cells with same F function based on heuristics
                if (fringe[i][2] > fringe[j][2]):
                    temp = fringe[j]
                    fringe[j] = fringe[i]
                    fringe[i] = temp
                            
    return fringe

# check if cell is in fringe already
def fringeCheck(cell,fringe):
    for i in range(len(fringe)):
        if ((fringe[i][0][0] == cell[0] and fringe[i][0][1] == cell[1])):
            return i
    return -1

def astarman(amanmap):
    s = [0,0]
    g = [dim-1,dim-1]

    maxNodesExp = 20
    nodeCounter = 0
    # array for tracking visited cells
    visited = mkVisAry(dim)
    
    # fringe of tuples containing coordinates and path to coordinate
    fringe = [(s, [], manhatDist([0,0],[dim-1,dim-1]))]  
    
    # loop while the fringe isn't empty
    while fringe:
        # sort the fringe by F cost = path length to cell (G cost) + euclidian distance remaining (H cost)
        fringeSort(fringe)
        cell, path, euc = fringe.pop(0)
        path.append(cell)
        visited[cell[0]][cell[1]] = True
        nodeCounter +=1
        if nodeCounter > maxNodesExp:
            -1
        

        if amanmap[cell[0]][cell[1]] == 'G':
            return len(path)

        neibs = getNeibs(cell[0],cell[1],dim)
        for neib in neibs:
            if visited[neib[0]][neib[1]] == False and amanmap[neib[0]][neib[1]] != 'X':
                elem = fringeCheck(neib,fringe)
                if (elem == -1):
                    fringe.append((neib, path[:], manhatDist([neib[0],neib[1]],[dim-1,dim-1])))
                else:
                    # if already in fringe with longer path, remove longer path and replace with shorter
                    if ((len(fringe[elem][1]) + fringe[elem][2]) > (len(path) + manhatDist([neib[0],neib[1]],[dim-1,dim-1]))):
                        fringe.remove(fringe[elem])
                        fringe.append((neib, path[:], manhatDist([neib[0],neib[1]],[dim-1,dim-1])))
                    # otherwise leave shorter path
                    else:
                        continue   
                
    # no path found            
    return -1  

print(astarman(maporig))
def alterMaze(maze):
    # Randomly add or remove an obstruction somewhere on the current maze
    # and solve.
    i = random.randint(1,2)
    randRow = random.randint(0,dim-1)
    randCol = random.randint(0, dim-1)
    
    if i == 1:
        # remove obstruction
        while maze[randRow][randCol] != 'X' :
            randRow = random.randint(0,dim-1)
            randCol = random.randint(0, dim-1)

        maze[randRow][randCol] = '-'
    else:
        while maze[randRow][randCol] != '-':
            randRow = random.randint(0,dim-1)
            randCol = random.randint(0, dim-1)

        maze[randRow][randCol] = 'X'
    return maze


def makeHarderMaze(dim, prob):
    # One maze for the first measure of hardness
    #   how long is the shortest path
    # Another for the second measure
    #   how many nodes were expanded

    # First measure Maze
    originalMaze = renderMap(dim, prob)
    # localSearchOne is going to return length of shortest path and maze
    #res = localSearchOne(originalMaze)
    res = astarman(originalMaze)
    #res = dfsMaxFringe(originalMaze)

    lengthShortP = res

    print("---Easier Maze---")
    print(np.matrix(originalMaze))

    start_time = time.time()
    if lengthShortP <= 0:
        # means no path was found and maze is most likely unsolvable
        while lengthShortP <= 0:
            originalMaze = renderMap(dim, prob)
            #res = localSearchOne(originalMaze)
            res = astarman(originalMaze)
            #res = dfsMaxFringe(originalMaze)
            lengthShortP = res
    
    newMaze1 = alterMaze(originalMaze)
    #newl = localSearchOne(newMaze1)
    newl = astarman(newMaze1)
    #newl = dfsMaxFringe(newMaze1)
    # If pathcount is negative or less than original shortest length
    #   keep looping
    if newl <= 0 or newl < lengthShortP:
        while newl <= 0 or newl < lengthShortP:
            newMaze1 = alterMaze(originalMaze)
            #newl = localSearchOne(newMaze1)
            newl = astarman(newMaze1)
            #newl = dfsMaxFringe(newMaze1)
    # newMaze1 is now the new maze from the first measure of hardness
    print("---%s seconds using A* Manhattan with max nodes---" %(time.time() - start_time))
    return newMaze1

hardMaze = makeHarderMaze(dim, prob)
print("---Hard maze generated---")
print(np.matrix(hardMaze)) 