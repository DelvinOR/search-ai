import random
import numpy as np
import math
from copy import copy, deepcopy

dim = 15
prob = .25

def renderMap (dim, prob):
    
    # Create dim x dim grid
    grid = []

    # Create dim rows
    for row in range(dim):
       # Create dim cols (2D Array)
        grid.append([])
        for col in range(dim):
            # Place values 
            if (prob < random.random()):
                grid[row].append('-')
            else:
                grid[row].append('X')
            
    grid[0][0] = 'S'
    grid[dim-1][dim-1] = 'G'
    
    return grid

map = renderMap(dim, prob)
maporig = deepcopy(map)

def euclidDist(fst, snd):
    dist = math.sqrt((fst[0]-snd[0])**2 + (fst[1]-snd[1])**2)
    return dist
    
def manhatDist(fst, snd):
    dist = float(abs(fst[0]-snd[0]) + abs(fst[1]-snd[1]))
    return dist
    
# get neighbors given current location and dim in order down, right, up,
# right visually from 0,0 top left to dim-1,dim-1 bottom right
def getNeibs(row, col, dim):
    # coordinates cant be less than 0 or greater than dim - 1 return none if so
    if (row < 0 or row > dim - 1 or col < 0 or col > dim -1):
        return
    # anywhere inside of the outer 'ring'
    if ((0 < row < dim - 1) and (0 < col < dim - 1)):
        neibs = [[row+1,col],[row,col+1],[row-1,col],[row,col-1]]
        return neibs
    # top row
    elif (row == 0):
        # top left corner
        if (col == 0):
            neibs = [[row+1,col],[row,col+1]]
            return neibs
        # top right corner
        elif (col == dim - 1):
            neibs = [[row+1,col],[row,col-1]]
            return neibs
        # rest of top row
        else:
            neibs = [[row+1,col],[row,col+1],[row,col-1]]
            return neibs
    # bottom row
    elif (row == dim - 1):
        # bottom left
        if (col == 0):
            neibs = [[row,col+1],[row-1,col]]
            return neibs
        # bottom right - commonly GOAL
        elif (col == dim-1):
            neibs = [[row-1,col],[row,col-1]]
            return neibs
        # rest of bottom row
        else:
            neibs = [[row,col+1],[row-1,col],[row,col-1]]
            return neibs
    # left column
    elif (col == 0):
        # top and bottom square already covered above
        neibs = [[row+1,col],[row,col+1],[row-1,col]]
        return neibs
    # last column
    else:
        #top and bottom square already covered above
        neibs = [[row+1,col],[row-1,col],[row,col-1]]
        return neibs

# end getNeibs

print("------ORIGINAL MAP------")
print(np.matrix(map))

# creates visited tracking array
def mkVisAry(dim):
    visited = []
    # Create dim rows
    for row in range(dim):
       # Create dim cols (2D Array)
        visited.append([])
        for col in range(dim):
            # Place values 
            visited[row].append(False)
    visited[0][0] = True
    return visited



# bfs
def bfs(s, g, map, dim):
    # array for tracking visited cells
    visited = mkVisAry(dim)
    visit = mkVisAry(dim)
    
    # fringe of tuples containing coordinates and path to coordinate
    fringe = [(s, [])]  
    
    # loop while the fringe isn't empty
    while fringe:
        cell, path = fringe.pop(0)
        path.append(cell)
        visited[cell[0]][cell[1]] = True

        if map[cell[0]][cell[1]] == 'G':
            #for i in range(dim):
                #for j in range(dim):
                    #if (visited[i][j] == True):
                        #visit[i][j] = 'T'
                    #else:
                        #visit[i][j] = 'F'
            #print(np.matrix(visit))
            return path

        neibs = getNeibs(cell[0],cell[1],dim)
        for neib in neibs:
            if visited[neib[0]][neib[1]] == False and map[neib[0]][neib[1]] != 'X':
                fringe.append((neib, path[:]))
    
    # no path found            
    return None  
    
bfsPath = bfs([0,0],[dim-1,dim-1],map,dim)

# map to show bfs result
bfsMap = map


# check if bfs path was found, if found
if (bfsPath != None):
    # change path cells to P to show path on matrix
    for step in bfsPath:
        bfsMap[step[0]][step[1]] = '*'
    print("------BFS PATH MAP------")
    print(np.matrix(bfsMap))
    
# path wasn't found
else:
    print("No Path Found")


print("------ORIGINAL MAP------")
print(np.matrix(maporig)) 
            
    
# dfs
def dfs(s, g, maporig, dim):
    # array for tracking visited cells
    visited = mkVisAry(dim)
    
    # fringe of tuples containing coordinates and path to coordinate
    fringe = [(s, [])] 
    
    # loop while the fringe isn't empty
    while fringe:
        cell, path = fringe.pop()
        path.append(cell)
        visited[cell[0]][cell[1]] = True

        if maporig[cell[0]][cell[1]] == 'G':
            #for i in range(dim):
                #for j in range(dim):
                    #if (visited[i][j] == True):
                        #visit[i][j] = 'T'
                    #else:
                        #visit[i][j] = 'F'
            #print(np.matrix(visit))
            return path

        neibs = getNeibs(cell[0],cell[1],dim)
        neibs.reverse()
        for neib in neibs:
            if visited[neib[0]][neib[1]] == False and map[neib[0]][neib[1]] != 'X':
                fringe.append((neib, path[:]))
    
    # no path found            
    return None  
    
dfsPath = dfs([0,0],[dim-1,dim-1],maporig,dim)
dfsMap = maporig

# check if bfs path was found, if found
if (dfsPath != None):
    # change path cells to P to show path on matrix
    for step in dfsPath:
        dfsMap[step[0]][step[1]] = '*'
    print("------DFS PATH MAP------")
    print(np.matrix(dfsMap))
    
# path wasn't found
else:
    print("No Path Found")

