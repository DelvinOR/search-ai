import random
import numpy as np
import math
import time
from copy import copy, deepcopy



# dimension of map
dim = 18
# probability of occupied cell
prob = .40
# starting cell
start = [0,0]
goal = [dim-1,dim-1]

# create square map given dim, prob, start, goal
def renderMap (dim, prob, start, goal):
    
    # Create dim x dim grid
    grid = []

    # Create dim rows
    for row in range(dim):
       # Create dim cols (2D Array)
        grid.append([])
        for col in range(dim):
            # Place values 
            if (prob < random.random()):
                grid[row].append('-')
            else:
                grid[row].append('X')
            
    grid[start[0]][start[1]] = 'S'
    grid[goal[0]][goal[1]] = 'G'
    
    return grid
    
# end create map

# get neighbors given current location and dim in order down, right, up,
# right visually from 0,0 top left to dim-1,dim-1 bottom right
def getNeibs(row, col, dim):
    # coordinates cant be less than 0 or greater than dim - 1 return none if so
    if (row < 0 or row > dim - 1 or col < 0 or col > dim -1):
        return
    # anywhere inside of the outer 'ring'
    if ((0 < row < dim - 1) and (0 < col < dim - 1)):
        neibs = [[row+1,col],[row,col+1],[row-1,col],[row,col-1]]
        return neibs
    # top row
    elif (row == 0):
        # top left corner
        if (col == 0):
            neibs = [[row+1,col],[row,col+1]]
            return neibs
        # top right corner
        elif (col == dim - 1):
            neibs = [[row+1,col],[row,col-1]]
            return neibs
        # rest of top row
        else:
            neibs = [[row+1,col],[row,col+1],[row,col-1]]
            return neibs
    # bottom row
    elif (row == dim - 1):
        # bottom left
        if (col == 0):
            neibs = [[row,col+1],[row-1,col]]
            return neibs
        # bottom right - commonly GOAL
        elif (col == dim-1):
            neibs = [[row-1,col],[row,col-1]]
            return neibs
        # rest of bottom row
        else:
            neibs = [[row,col+1],[row-1,col],[row,col-1]]
            return neibs
    # left column
    elif (col == 0):
        # top and bottom square already covered above
        neibs = [[row+1,col],[row,col+1],[row-1,col]]
        return neibs
    # last column
    else:
        #top and bottom square already covered above
        neibs = [[row+1,col],[row-1,col],[row,col-1]]
        return neibs

# end getNeibs

# creates visited tracking array
def mkVisAry(dim):
    visited = []
    # Create dim rows
    for row in range(dim):
       # Create dim cols (2D Array)
        visited.append([])
        for col in range(dim):
            # Place values 
            visited[row].append(False)
    visited[0][0] = True
    return visited
    
# end mkVisAry

# fringe sort based on F function and heuristic tie breaker
def fringeSort(fringe):
    # sort fringe based on F function = sum of path length to cell and heuristic
    fringe.sort(key = lambda x: len(x[1]) + x[2])
    # sort ties using heuristic as tie breaker
    for i in range(0,len(fringe)-1):
        for j in range(i+1,len(fringe)):
            # F functions are the same
            if ((len(fringe[i][1]) + fringe[i][2]) == (len(fringe[j][1]) + fringe[j][2])):
                # order cells with same F function based on heuristics
                if (fringe[i][2] > fringe[j][2]):
                    temp = fringe[j]
                    fringe[j] = fringe[i]
                    fringe[i] = temp
                            
    return fringe

# check if cell is in fringe already
def fringeCheck(cell,fringe):
    for i in range(len(fringe)):
        if ((fringe[i][0][0] == cell[0] and fringe[i][0][1] == cell[1])):
            return i
    return -1

#------START OF A* Manhattan------"


# Manhattan Distance from any point in grid to goal
def manhatDist(fst, dim):
    dist = float(abs(fst[0]-(dim-1)) + abs(fst[1]-(dim-1)))
    return dist

# a star with manhattan distance heuristic
def astarman(s, g, amanmap, dim):
    # array for tracking visited cells
    visited = mkVisAry(dim)
    
    # fringe of tuples containing coordinates and path to coordinate
    fringe = [(s, [], manhatDist([0,0],dim))]  
    
    # loop while the fringe isn't empty
    while fringe:
        # sort the fringe by F cost = path length to cell (G cost) + euclidian distance remaining (H cost)
        fringeSort(fringe)
        cell, path, euc = fringe.pop(0)
        path.append(cell)
        visited[cell[0]][cell[1]] = True

        if amanmap[cell[0]][cell[1]] == 'G':
            return path

        neibs = getNeibs(cell[0],cell[1],dim)
        for neib in neibs:
            if visited[neib[0]][neib[1]] == False and amanmap[neib[0]][neib[1]] != 'X':
                elem = fringeCheck(neib,fringe)
                if (elem == -1):
                    fringe.append((neib, path[:], manhatDist([neib[0],neib[1]],dim)))
                else:
                    # if already in fringe with longer path, remove longer path and replace with shorter
                    if ((len(fringe[elem][1]) + fringe[elem][2]) > (len(path) + manhatDist([neib[0],neib[1]],dim))):
                        fringe.remove(fringe[elem])
                        fringe.append((neib, path[:], manhatDist([neib[0],neib[1]],dim)))
                    # otherwise leave shorter path
                    else:
                        continue   
                
    # no path found            
    return None  


print("Manhattan Time")
p = 0
while (p < .31):
    i = 0
    timesum = 0
    while (i < 100):
        # create the map    
        map = renderMap(dim, p, start, goal) 
        beg = time.time() 
        astar = astarman(start,goal,map,dim)
        end = time.time()
        i = i + 1
        timesum = timesum + (end - beg)
    print(p, timesum)
    p = p + .01
    
#------START OF A* Euclidean------"


# Euclidean Distance from any point in grid to goal
def euclidDist(fst, dim):
    dist = math.sqrt((fst[0]-(dim-1))**2 + (fst[1]-(dim-1))**2)
    return dist
    
# a star with euclidean distance heuristic
def astareuc(s, g, aeucmap, dim):
    # array for tracking visited cells
    visited = mkVisAry(dim)
    
    # fringe of tuples containing coordinates and path to coordinate
    fringe = [(s, [], euclidDist([0,0],dim))]  
    
    # loop while the fringe isn't empty
    while fringe:
        # sort the fringe by F cost = path length to cell (G cost) + euclidian distance remaining (H cost)
        fringeSort(fringe)
        cell, path, euc = fringe.pop(0)
        path.append(cell)
        visited[cell[0]][cell[1]] = True

        if aeucmap[cell[0]][cell[1]] == 'G':
            return path

        neibs = getNeibs(cell[0],cell[1],dim)
        for neib in neibs:
            if visited[neib[0]][neib[1]] == False and aeucmap[neib[0]][neib[1]] != 'X':
                elem = fringeCheck(neib,fringe)
                if (elem == -1):
                    fringe.append((neib, path[:], euclidDist([neib[0],neib[1]],dim)))
                else:
                    # if already in fringe with longer path, remove longer path and replace with shorter
                    if ((len(fringe[elem][1]) + fringe[elem][2]) > (len(path) + euclidDist([neib[0],neib[1]],dim))):
                        fringe.remove(fringe[elem])
                        fringe.append((neib, path[:], euclidDist([neib[0],neib[1]],dim)))
                    # otherwise leave shorter path
                    else:
                        continue   
                
    # no path found            
    return None  

print("Euclidean Time")
p = 0
while (p < .31):
    i = 0
    timesum = 0
    while (i < 100):
        # create the map    
        map = renderMap(dim, p, start, goal) 
        beg = time.time() 
        astar = astareuc(start,goal,map,dim)
        end = time.time()
        i = i + 1
        timesum = timesum + (end - beg)
    print(timesum)
    p = p + .01
    
#------END OF A* Euclidean------"






