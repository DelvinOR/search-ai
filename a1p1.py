import random
import numpy as np
import math
from copy import copy, deepcopy


dimin = input("Enter dimension of map: ")
if (0 < dimin < 50):
    dim = dimin
else:
    # dimension of map 15 default/incorrect input
    dim = 15
    
probin = input("Enter probability in decimal: ")
if (0 <= probin < 1):
    prob = probin
else:
    # probability of occupied cell default .35
    prob = .35
# starting cell
start = [0,0]
goal = [dim-1,dim-1]

# create square map given dim, prob, start, goal
def renderMap (dim, prob, start, goal):
    
    # Create dim x dim grid
    grid = []

    # Create dim rows
    for row in range(dim):
       # Create dim cols (2D Array)
        grid.append([])
        for col in range(dim):
            # Place values 
            if (prob < random.random()):
                grid[row].append('-')
            else:
                grid[row].append('X')
            
    grid[start[0]][start[1]] = 'S'
    grid[goal[0]][goal[1]] = 'G'
    
    return grid
    
# end create map

# get neighbors given current location and dim in order down, right, up,
# right visually from 0,0 top left to dim-1,dim-1 bottom right
def getNeibs(row, col, dim):
    # coordinates cant be less than 0 or greater than dim - 1 return none if so
    if (row < 0 or row > dim - 1 or col < 0 or col > dim -1):
        return
    # anywhere inside of the outer 'ring'
    if ((0 < row < dim - 1) and (0 < col < dim - 1)):
        neibs = [[row+1,col],[row,col+1],[row-1,col],[row,col-1]]
        return neibs
    # top row
    elif (row == 0):
        # top left corner
        if (col == 0):
            neibs = [[row+1,col],[row,col+1]]
            return neibs
        # top right corner
        elif (col == dim - 1):
            neibs = [[row+1,col],[row,col-1]]
            return neibs
        # rest of top row
        else:
            neibs = [[row+1,col],[row,col+1],[row,col-1]]
            return neibs
    # bottom row
    elif (row == dim - 1):
        # bottom left
        if (col == 0):
            neibs = [[row,col+1],[row-1,col]]
            return neibs
        # bottom right - commonly GOAL
        elif (col == dim-1):
            neibs = [[row-1,col],[row,col-1]]
            return neibs
        # rest of bottom row
        else:
            neibs = [[row,col+1],[row-1,col],[row,col-1]]
            return neibs
    # left column
    elif (col == 0):
        # top and bottom square already covered above
        neibs = [[row+1,col],[row,col+1],[row-1,col]]
        return neibs
    # last column
    else:
        #top and bottom square already covered above
        neibs = [[row+1,col],[row-1,col],[row,col-1]]
        return neibs

# end getNeibs

# creates visited tracking array
def mkVisAry(dim):
    visited = []
    # Create dim rows
    for row in range(dim):
       # Create dim cols (2D Array)
        visited.append([])
        for col in range(dim):
            # Place values 
            visited[row].append(False)
    visited[0][0] = True
    return visited
    
# end mkVisAry



#------START OF DFS------#

# dfs
def dfs(s, g, dfsmap, dim):
    # array for tracking visited cells
    visited = mkVisAry(dim)
    
    # fringe of tuples containing coordinates and path to coordinate
    fringe = [(s, [])] 
    
    # loop while the fringe isn't empty
    while fringe:
        cell, path = fringe.pop()
        path.append(cell)
        visited[cell[0]][cell[1]] = True

        if dfsmap[cell[0]][cell[1]] == 'G':
            #for i in range(dim):
                #for j in range(dim):
                    #if (visited[i][j] == True):
                        #visit[i][j] = 'T'
                    #else:
                        #visit[i][j] = 'F'
            #print(np.matrix(visit))
            return path

        neibs = getNeibs(cell[0],cell[1],dim)
        neibs.reverse()
        for neib in neibs:
            if visited[neib[0]][neib[1]] == False and dfsmap[neib[0]][neib[1]] != 'X':
                fringe.append((neib, path[:]))
    
    # no path found            
    return None  

# create the map    
map = renderMap(dim, prob, start, goal)
# copy the map for dfs search
dfsmap = deepcopy(map)    

# if no dfs path, make a new map and try again until map is solvable 
dfsPath = dfs(start,goal,dfsmap,dim)

while (dfsPath == None):
    print("No Path Found: Generating New Map")
    map = renderMap(dim, prob, start, goal)
    dfsmap = deepcopy(map)
    dfsPath = dfs(start,goal,dfsmap,dim)

# create copy of map to show path on
dfsMap = dfsmap

# change path cells to P to show path on matrix
for step in dfsPath:
    dfsMap[step[0]][step[1]] = '*'
print("------ORIGINAL MAP------")
print(np.matrix(map))
print("------DFS PATH MAP------")
print(np.matrix(dfsMap))
print("------DFS PATH------")
print(dfsPath)
print("------DFS PATH LENGTH------")
print(len(dfsPath))


    
#------END OF DFS------#

#------START OF BFS------"

# copy the map for bfs search
bfsmap = deepcopy(map)  

# bfs
def bfs(s, g, bfsmap, dim):
    # array for tracking visited cells
    visited = mkVisAry(dim)
    
    # fringe of tuples containing coordinates and path to coordinate
    fringe = [(s, [])]  
    
    # loop while the fringe isn't empty
    while fringe:
        cell, path = fringe.pop(0)
        path.append(cell)
        visited[cell[0]][cell[1]] = True

        if bfsmap[cell[0]][cell[1]] == 'G':
            return path

        neibs = getNeibs(cell[0],cell[1],dim)
        for neib in neibs:
            if visited[neib[0]][neib[1]] == False and bfsmap[neib[0]][neib[1]] != 'X':
                fringe.append((neib, path[:]))
    
    # no path found            
    return None  
    
bfsPath = bfs(start,goal,bfsmap,dim)

# map to show bfs result
bfsMap = bfsmap


# check if bfs path was found, if found
if (bfsPath != None):
    # change path cells to P to show path on matrix
    for step in bfsPath:
        bfsMap[step[0]][step[1]] = '*'
    print("------BFS PATH MAP------")
    print(np.matrix(bfsMap))
    print("------BFS PATH------")
    print(bfsPath)
    print("------BFS PATH LENGTH------")
    print(len(bfsPath))

    
# path wasn't found
else:
    print("No Path Found")

#------END OF BFS------#

# fringe sort based on F function and heuristic tie breaker
def fringeSort(fringe):
    # sort fringe based on F function = sum of path length to cell and heuristic
    fringe.sort(key = lambda x: len(x[1]) + x[2])
    # sort ties using heuristic as tie breaker
    for i in range(0,len(fringe)-1):
        for j in range(i+1,len(fringe)):
            # F functions are the same
            if ((len(fringe[i][1]) + fringe[i][2]) == (len(fringe[j][1]) + fringe[j][2])):
                # order cells with same F function based on heuristics
                if (fringe[i][2] > fringe[j][2]):
                    temp = fringe[j]
                    fringe[j] = fringe[i]
                    fringe[i] = temp
                            
    return fringe

# check if cell is in fringe already
def fringeCheck(cell,fringe):
    for i in range(len(fringe)):
        if ((fringe[i][0][0] == cell[0] and fringe[i][0][1] == cell[1])):
            return i
    return -1
    
#------START OF A* Euclidean------"

# copy the map for A* Euclidean search
aeucmap = deepcopy(map)  

# Euclidean Distance from any point in grid to goal
def euclidDist(fst, dim):
    dist = math.sqrt((fst[0]-(dim-1))**2 + (fst[1]-(dim-1))**2)
    return dist
    
# a star with euclidean distance heuristic
def astareuc(s, g, aeucmap, dim):
    # array for tracking visited cells
    visited = mkVisAry(dim)
    
    # fringe of tuples containing coordinates and path to coordinate
    fringe = [(s, [], euclidDist([0,0],dim))]  
    
    # loop while the fringe isn't empty
    while fringe:
        # sort the fringe by F cost = path length to cell (G cost) + euclidian distance remaining (H cost)
        fringeSort(fringe)
        cell, path, euc = fringe.pop(0)
        path.append(cell)
        visited[cell[0]][cell[1]] = True

        if aeucmap[cell[0]][cell[1]] == 'G':
            return path

        neibs = getNeibs(cell[0],cell[1],dim)
        for neib in neibs:
            if visited[neib[0]][neib[1]] == False and aeucmap[neib[0]][neib[1]] != 'X':
                elem = fringeCheck(neib,fringe)
                if (elem == -1):
                    fringe.append((neib, path[:], euclidDist([neib[0],neib[1]],dim)))
                else:
                    # if already in fringe with longer path, remove longer path and replace with shorter
                    if ((len(fringe[elem][1]) + fringe[elem][2]) > (len(path) + euclidDist([neib[0],neib[1]],dim))):
                        fringe.remove(fringe[elem])
                        fringe.append((neib, path[:], euclidDist([neib[0],neib[1]],dim)))
                    # otherwise leave shorter path
                    else:
                        continue   
                
    # no path found            
    return None  
    
astareucPath = astareuc(start,goal,aeucmap,dim)
astareucMap = aeucmap

# check if A* euclidean path was found, if found
if (astareucPath != None):
    # change path cells to P to show path on matrix
    for step in astareucPath:
        astareucMap[step[0]][step[1]] = '*'
    print("------A* Euclidean PATH MAP------")
    print(np.matrix(astareucMap))
    print("------A* Euclidean PATH------")
    print(astareucPath)
    print("------A* Euclidean PATH LENGTH------")
    print(len(astareucPath))

    
# path wasn't found
else:
    print("No Path Found")
    
#------END OF A* Euclidean------"

#------START OF A* Manhattan------"

# copy the map for A* Manhattan search
amanmap = deepcopy(map) 

# Manhattan Distance from any point in grid to goal
def manhatDist(fst, dim):
    dist = float(abs(fst[0]-(dim-1)) + abs(fst[1]-(dim-1)))
    return dist

# a star with manhattan distance heuristic
def astarman(s, g, amanmap, dim):
    # array for tracking visited cells
    visited = mkVisAry(dim)
    
    # fringe of tuples containing coordinates and path to coordinate
    fringe = [(s, [], manhatDist([0,0],dim))]  
    
    # loop while the fringe isn't empty
    while fringe:
        # sort the fringe by F cost = path length to cell (G cost) + euclidian distance remaining (H cost)
        fringeSort(fringe)
        cell, path, euc = fringe.pop(0)
        path.append(cell)
        visited[cell[0]][cell[1]] = True

        if amanmap[cell[0]][cell[1]] == 'G':
            return path

        neibs = getNeibs(cell[0],cell[1],dim)
        for neib in neibs:
            if visited[neib[0]][neib[1]] == False and amanmap[neib[0]][neib[1]] != 'X':
                elem = fringeCheck(neib,fringe)
                if (elem == -1):
                    fringe.append((neib, path[:], manhatDist([neib[0],neib[1]],dim)))
                else:
                    # if already in fringe with longer path, remove longer path and replace with shorter
                    if ((len(fringe[elem][1]) + fringe[elem][2]) > (len(path) + manhatDist([neib[0],neib[1]],dim))):
                        fringe.remove(fringe[elem])
                        fringe.append((neib, path[:], manhatDist([neib[0],neib[1]],dim)))
                    # otherwise leave shorter path
                    else:
                        continue   
                
    # no path found            
    return None  
    
astarmanPath = astarman(start,goal,amanmap,dim)
astarmanMap = amanmap

# check if bfs path was found, if found
if (astareucPath != None):
    # change path cells to P to show path on matrix
    for step in astareucPath:
        astarmanMap[step[0]][step[1]] = '*'
    print("------A* Manhattan PATH MAP------")
    print(np.matrix(astarmanMap))
    print("------A* Manhattan PATH------")
    print(astarmanPath)
    print("------A* Manhattan PATH LENGTH------")
    print(len(astarmanPath))

    
# path wasn't found
else:
    print("No Path Found")

#------END OF A* Manhattan------"


#------START OF Bi-Directional BFS------"

# copy the map for Bi-Directional BFS search
bfsbimap = deepcopy(map) 

# bi directional bfs
def bfsbi(s, g, bfsbimap, dim):
    # array for tracking visited cells
    visited = mkVisAry(dim)
    visited2 = mkVisAry(dim)
    
    # fringe of tuples containing coordinates and path to coordinate
    fringe = [(s, [])]  
    fringe2 = [(g, [])]
    
    # loop while the fringe isn't empty
    while fringe and fringe2:
        
        if fringe:
            cell, path = fringe.pop(0)
            path.append(cell)
            visited[cell[0]][cell[1]] = True
            if (bfsmap[cell[0]][cell[1]] == 'G'):
                return path
            # check if cell is in the other fringe
            frnck = fringeCheck(cell,fringe2)
            if (frnck != -1):
                endpath = fringe2[frnck][1]
                endpath.reverse()
                return path + endpath
            # get neighbors for current cells
            neibs = getNeibs(cell[0],cell[1],dim)   
            # add neighbors to fringe
            for neib in neibs:
                if visited[neib[0]][neib[1]] == False and bfsbimap[neib[0]][neib[1]] != 'X':
                    fringe.append((neib, path[:]))
            
            
            
        if fringe2:
            cell2, path2 = fringe2.pop(0)
            path2.append(cell2)
            visited2[cell2[0]][cell2[1]] = True
            if (bfsmap[cell2[0]][cell2[1]] == 'G'):
                return path2
            # check if cell is in the other fringe
            frnck2 = fringeCheck(cell2,fringe)
            if (frnck2 != -1):
                path2.reverse()
                begpath = fringe[frnck2][1]
                return begpath + path2
            neibs2 = getNeibs(cell2[0],cell2[1],dim)
             # add neighbors to fringe in reverse order since working from other direction
            neibs2.reverse()
            for neib2 in neibs2:
                if visited2[neib2[0]][neib2[1]] == False and bfsbimap[neib2[0]][neib2[1]] != 'X':
                    fringe2.append((neib2, path2[:]))
       
    
    # no path found            
    return None  
    
bfsbiPath = bfsbi(start,goal,bfsbimap,dim)


# map to show bfs result
bfsbiMap = bfsbimap


# check if bfs path was found, if found
if (bfsbiPath != None):
    # change path cells to P to show path on matrix
    for step in bfsbiPath:
        bfsbiMap[step[0]][step[1]] = '*'
    print("------BI-DIRECTIONAL BFS PATH MAP------")
    print(np.matrix(bfsbiMap))
    print("------BI-DIRECTIONAL BFS PATH------")
    print(bfsbiPath)
    print("------BI-DIRECTIONAL PATH LENGTH------")
    print(len(bfsbiPath))

    
# path wasn't found
else:
    print("No Path Found")
    






