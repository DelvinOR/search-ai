import random
import numpy as np
import math
from copy import copy, deepcopy



# dimension of map
dim = 18
# probability of occupied cell
prob = .40
# starting cell
start = [0,0]
goal = [dim-1,dim-1]

# create square map given dim, prob, start, goal
def renderMap (dim, prob, start, goal):
    
    # Create dim x dim grid
    grid = []

    # Create dim rows
    for row in range(dim):
       # Create dim cols (2D Array)
        grid.append([])
        for col in range(dim):
            # Place values 
            if (prob < random.random()):
                grid[row].append('-')
            else:
                grid[row].append('X')
            
    grid[start[0]][start[1]] = 'S'
    grid[goal[0]][goal[1]] = 'G'
    
    return grid
    
# end create map

# get neighbors given current location and dim in order down, right, up,
# right visually from 0,0 top left to dim-1,dim-1 bottom right
def getNeibs(row, col, dim):
    # coordinates cant be less than 0 or greater than dim - 1 return none if so
    if (row < 0 or row > dim - 1 or col < 0 or col > dim -1):
        return
    # anywhere inside of the outer 'ring'
    if ((0 < row < dim - 1) and (0 < col < dim - 1)):
        neibs = [[row+1,col],[row,col+1],[row-1,col],[row,col-1]]
        return neibs
    # top row
    elif (row == 0):
        # top left corner
        if (col == 0):
            neibs = [[row+1,col],[row,col+1]]
            return neibs
        # top right corner
        elif (col == dim - 1):
            neibs = [[row+1,col],[row,col-1]]
            return neibs
        # rest of top row
        else:
            neibs = [[row+1,col],[row,col+1],[row,col-1]]
            return neibs
    # bottom row
    elif (row == dim - 1):
        # bottom left
        if (col == 0):
            neibs = [[row,col+1],[row-1,col]]
            return neibs
        # bottom right - commonly GOAL
        elif (col == dim-1):
            neibs = [[row-1,col],[row,col-1]]
            return neibs
        # rest of bottom row
        else:
            neibs = [[row,col+1],[row-1,col],[row,col-1]]
            return neibs
    # left column
    elif (col == 0):
        # top and bottom square already covered above
        neibs = [[row+1,col],[row,col+1],[row-1,col]]
        return neibs
    # last column
    else:
        #top and bottom square already covered above
        neibs = [[row+1,col],[row-1,col],[row,col-1]]
        return neibs

# end getNeibs

# creates visited tracking array
def mkVisAry(dim):
    visited = []
    # Create dim rows
    for row in range(dim):
       # Create dim cols (2D Array)
        visited.append([])
        for col in range(dim):
            # Place values 
            visited[row].append(False)
    visited[0][0] = True
    return visited
    
# end mkVisAry

#------START OF DFS------#

# dfs
def dfs(s, g, dfsmap, dim):
    # array for tracking visited cells
    visited = mkVisAry(dim)
    
    # fringe of tuples containing coordinates and path to coordinate
    fringe = [(s, [])] 
    
    # loop while the fringe isn't empty
    while fringe:
        cell, path = fringe.pop()
        path.append(cell)
        visited[cell[0]][cell[1]] = True

        if dfsmap[cell[0]][cell[1]] == 'G':
            #for i in range(dim):
                #for j in range(dim):
                    #if (visited[i][j] == True):
                        #visit[i][j] = 'T'
                    #else:
                        #visit[i][j] = 'F'
            #print(np.matrix(visit))
            return path

        neibs = getNeibs(cell[0],cell[1],dim)
        neibs.reverse()
        for neib in neibs:
            if visited[neib[0]][neib[1]] == False and dfsmap[neib[0]][neib[1]] != 'X':
                fringe.append((neib, path[:]))
    
    # no path found            
    return None  


p = 0
while (p < 1):
    i = 0
    count = 0
    while (i < 1000):
        # create the map    
        map = renderMap(dim, p, start, goal)  
        dfsPath = dfs(start,goal,map,dim)
        if dfsPath != None:
            count = count + 1;
        i = i + 1
    print(p, count)
    p = p + .01






